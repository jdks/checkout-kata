class Criteria
  def initialize(quantity: 0, total: 0.00, product_code: nil)
    @quantity = quantity
    @total = total
    @product_code = product_code
    freeze
  end

  def match?(basket)
    [
      basket.quantity_of(product_code) >= quantity,
      basket.total >= total,
    ].compact.inject(true, :&)
  end

  attr_reader :quantity, :total, :product_code
end
