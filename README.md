# Checkout Kata

An exercise in creating a checkout that accepts promotional rules and produces a discount price for a set of items.

```ruby
co = Checkout.new(promotional_rules)
co.scan(item)
co.scan(item)
price = co.total
```

## Running the tests

Please install the dependencies using bundler.

    bundle install

Requirements are described within cucumber features and can be run with the following command.

    bundle exec cucumber

Unit tests are written RSpec.

    bundle exec rspec