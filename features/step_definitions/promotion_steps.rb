Given /if you spend over £60/ do
  add_promotion_criteria(total: 60.00)
end

Given /if you buy 2 or more travel card holders/ do
  add_promotion_criteria(quantity: 2, product_code: '001')
end

Then /you get 10% off your purchase/ do
  add_promotion_discount(value: 10.00, type: :percentage)
end

Then /the price drops to £8\.50/ do
  add_promotion_discount(value: 8.50, type: :fixed_amount)
end

When /I have the following items in the basket:/ do |table|
  table.raw.each do |column|
    column.each do |product_code|
      item = find_item(product_code)
      checkout.scan(item)
    end
  end
end

Then /the total price expected is (£.+)/ do |expected_price|
  expect(checkout.total).to eql(expected_price)
end
