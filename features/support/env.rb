require './promotion'
require './criteria'
require './discount'
require './products'
require './checkout'
require './helpful_array'

module Driver
  def checkout
    @checkout ||= Checkout.new(promotion_rules)
  end

  def find_item(product_code)
    Products.find(product_code)
  end

  def add_promotion_criteria(options)
    promotion.criteria = Criteria.new(options)
  end

  def add_promotion_discount(options)
    promotion.discount = Discount.new(options)
    promotion_rules << promotion
    @promotion = nil
  end

  def promotion
    @promotion ||= Promotion.new
  end

  def promotion_rules
    @promotion_rules ||= []
  end
end

World(Driver)
