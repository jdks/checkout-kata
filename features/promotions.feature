Feature: Promotion

  Background:
    Given if you spend over £60
    Then you get 10% off your purchase
    And if you buy 2 or more travel card holders
    Then the price drops to £8.50

  Scenario: Over £60 spending
    When I have the following items in the basket:
      | 001 | 002 | 003 |
    Then the total price expected is £66.78

  Scenario: Two travel card holders
    When I have the following items in the basket:
      | 001 | 003 | 001 |
    Then the total price expected is £36.95

  Scenario: Over £60 and two travel card holders
    When I have the following items in the basket:
      | 001 | 002 | 001 | 003 |
    Then the total price expected is £73.76
