class Item < Struct.new(:product_code, :name, :price)
  def initialize(*args)
    super(*args)
    freeze
  end

  def self_with_index
    { product_code => self }
  end
end
