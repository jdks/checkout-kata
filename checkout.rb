require './basket'

class Checkout
  extend Forwardable

  def_delegator :basket, :update, :scan

  def initialize(promotion_rules)
    @promotion_rules = promotion_rules
  end

  def total
    '£%.2f' % (basket.total - discount)
  end

  private

  attr_reader :promotion_rules

  def basket
    @basket ||= Basket.new
  end

  def discount
    basket.calculate_discount(active_promotions)
  end

  def active_promotions
    promotion_rules.select(&eligible?)
  end

  def eligible?
    lambda do |promotion|
      promotion.criteria_match?(basket)
    end
  end
end
