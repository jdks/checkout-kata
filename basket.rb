require './helpful_array'

class Basket
  attr_accessor :items

  def initialize(items = [])
    @items = HelpfulArray.new(items)
  end

  def update(item)
    tap { items << item }
  end

  def quantity_of(product_code)
    product(product_code).size
  end

  def product(product_code)
    items.where(product_code: product_code)
  end

  def calculate_discount(promotions)
    promotions.inject(0) do |discount, promotion|
      discounted_items = product(promotion.criteria.product_code)
      discount += promotion.discount.for(discounted_items, total)
    end
  end

  def total
    items.sum(:price)
  end
end
