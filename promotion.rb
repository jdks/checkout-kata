class Promotion
  attr_accessor :criteria, :discount

  def initialize(criteria: nil, discount: nil)
    @criteria = criteria
    @discount = discount
  end

  def criteria_match?(basket)
    criteria.match?(basket) if criteria
  end

  def fixed_amount?
    criteria.type == :fixed_amount
  end
end
