class Discount
  def initialize(value:, type:)
    @value = value
    @type = type
    freeze
  end

  def for(items, total)
    case type
    when :fixed_amount; items.map(&:price).inject(:+) - items.size * value
    when :percentage;  total * value / 100
    else 0.00
    end
  end

  private

  attr_reader :value, :type
end
