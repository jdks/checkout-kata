class HelpfulArray < Array
  def where(options)
    select do |item|
      options.inject(true) do |result, (name, value)|
        result && item.method(name)[] == value
      end
    end
  end

  def find(options)
    where(options).first
  end

  def sum(name)
    map(&name).inject(0, :+)
  end
end
