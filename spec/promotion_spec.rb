require './promotion'

RSpec.describe Promotion do
  let(:promotion) { Promotion.new(criteria: criteria) }
  
  let(:criteria) { double('Criteria') }

  it 'has expected accessors' do
    expect(promotion).to match(
      an_object_having_attributes(
        criteria: criteria,
        discount: nil
      )
    )
  end

  describe '#criteria_match?' do
    subject { promotion.criteria_match?(basket) }

    let(:basket) { double('Basket') }

    before do
      if criteria
        allow(criteria).to receive(:match?).with(basket).
          and_return(match?)
      end
    end

    context 'basket fulfils criteria' do
      let(:match?) { true }

      it { is_expected.to be(true) }
    end

    context 'basket does not fulfil criteria' do
      let(:match?) { false }

      it { is_expected.to be(false) }
    end

    context 'no criteria' do
      let(:criteria) { nil }

      it { is_expected.to eql(nil) }
    end
  end
end
