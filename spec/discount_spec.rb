require './discount'

RSpec.describe Discount do
  let(:discount) { Discount.new(value: 10.00, type: discount_type) }

  describe '#for' do
    subject { discount.for(items, total) }

    let(:items) { [item] * 3 }
    let(:item) { double('Item', price: 15.00) }
    let(:total) { 45.00 }

    context 'fixed amount' do
      let(:discount_type) { :fixed_amount }


      it { is_expected.to eql(15.00) }
    end

    context 'percentage' do
      let(:discount_type) { :percentage }
      
      it { is_expected.to eql(4.50) }
    end

    context 'other' do
      let(:discount_type) { :erroneous }

      it { is_expected.to eql(0.00) }
    end
  end
end
