require './basket'

RSpec.describe Basket do
  let(:basket) { Basket.new(fake_items) }

  let(:fake_items) { [] }

  describe '#update' do
    subject { basket.update(item) }

    let(:item) { double('Item', price: 10.00) }

    it 'adds a new item' do
      expect { subject }.to change(basket, :items).from([]).to([item])
    end

    it 'updates the total value' do
      expect { subject }.to change(basket, :total).by(item.price)
    end

    it 'returns the basket' do
      expect(subject).to eql(basket)
    end
  end

  describe '#calculate_discount' do
    subject { basket.calculate_discount(promotions) }

    let(:discount) { double('Discount', value: 5.00, type: :percentage) }
    let(:criteria) { double('Criteria', product_code: nil) }
    let(:promotion) { double('Promotion', criteria: criteria, discount: discount) }

    let(:promotions) { [promotion] }
    let(:fake_items) { [item] }
    let(:item) { double('Item', price: 30.00, product_code: '011') }

    before do
      allow(discount).to receive(:for).with([], 30.00).
        and_return(5.00)
    end

    it 'calculates the discount based on the items in the basket' do
      expect(subject).to eql(5.00)
    end
  end

  describe '#total' do
    subject { basket.total }

    let(:fake_items) {
      [
        double('Item', price: 7.00),
        double('Item', price: 8.00),
        double('Item', price: 13.00),
      ]
    }

    it 'returns the total price' do
      expect(subject).to eql(28.00)
    end
  end

  describe '#quantity_of' do
    subject { basket.quantity_of(product_code) }

    let(:fake_items) {
      [
        double('Item', product_code: '011'),
        double('Item', product_code: '022'),
        double('Item', product_code: '022'),
      ]
    }

    context 'one item' do
      let(:product_code) { '011' }

      it { is_expected.to eql(1) }
    end

    context 'two items' do
      let(:product_code) { '022' }

      it { is_expected.to eql(2) }
    end

    context 'no items' do
      let(:product_code) { '000' }

      it { is_expected.to eql(0) }
    end
  end
end
