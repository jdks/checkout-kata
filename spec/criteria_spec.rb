require './criteria'

RSpec.describe Criteria do
  let(:criteria) { Criteria.new(options) }
  let(:basket) { double('Basket', total: 30.00) }
  
  describe '#match?' do
    subject { criteria.match?(basket) }

    let(:product_code) { '001' }
    let(:basket_quantity) { 4 }

    before do
      allow(basket).to receive(:quantity_of).with(product_code).
        and_return(basket_quantity)
    end

    context 'quantity match' do
      let(:options) { { quantity: 3, product_code: product_code } }

      it { is_expected.to be(true) }

      context 'not enough items' do
        let(:basket_quantity) { 2 }

        it { is_expected.to be(false) }
      end
    end

    context 'total price match' do
      let(:options) { { total: 20.00 } }
      let(:product_code) { nil }

      it { is_expected.to be(true) }

      context 'basket total too small' do
        let(:options) { { total: 60.00 } }

        it { is_expected.to be(false) }
      end
    end
  end
end
