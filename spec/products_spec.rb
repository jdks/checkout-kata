require './products'

RSpec.describe Products do
  describe '.find' do
    subject { Products.find(product_code) }

    let(:product_code) { '003' }
    let(:item1) { double('Item') }
    let(:item2) { double('Item') }
    let(:item3) { double('Item') }

    before do
      allow(Item).to receive(:new).and_return(item1, item2, item3)
      allow(item1).to receive(:self_with_index).and_return('001' => item1)
      allow(item2).to receive(:self_with_index).and_return('002' => item2)
      allow(item3).to receive(:self_with_index).and_return('003' => item3)
    end

    it 'finds a product by is product code' do
      expect(subject).to eql(item3)
    end

    context 'product code not available' do
      let(:product_code) { '101' }

      it 'raises an error' do
        expect { subject }.to raise_error(Products::ItemNotFoundError)
      end
    end
  end
end
