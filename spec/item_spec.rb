require './item'

RSpec.describe Item do
  subject(:item) { Item.new('004', 'T-shirt', 12.99) }

  it 'has expected attributes' do
    expect(item).to match(
      an_object_having_attributes(
        product_code: '004',
        name: 'T-shirt',
        price: 12.99
      )
    )
  end

  describe '#self_with_index' do
    subject { item.self_with_index }

    it { is_expected.to eql('004' => item) }
  end
end
