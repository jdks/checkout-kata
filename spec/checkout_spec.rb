require './checkout'

RSpec.describe Checkout do
  let(:checkout) { Checkout.new(promotion_rules) }

  let(:basket) { double('Basket') }
  let(:promotion_rules) { [] }

  before do
    allow(Basket).to receive(:new).and_return(basket)
  end

  describe '#scan' do
    let(:item) { double('Item') }

    it 'adds an item to the basket' do
      expect(basket).to receive(:update).with(item)

      checkout.scan(item)
    end
  end

  describe '#total' do
    subject { checkout.total }

    before do
      allow(basket).to receive(:calculate_discount).and_return(10)
      allow(basket).to receive(:total).and_return(60.50)
    end

    it 'delegates to the basket' do
      expect(subject).to eql('£50.50')
    end
  end
end
