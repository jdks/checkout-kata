require 'singleton'
require './item'

class Products
  ItemNotFoundError = Class.new(StandardError)

  include Singleton

  def self.find(code)
    instance.find(code)
  end

  def find(code)
    stock.fetch(code)
  rescue KeyError
    raise ItemNotFoundError, "Item with product code '#{code}' not found"
  end

  private

  def stock
    @stock ||= items.map(&:self_with_index).inject(:merge)
  end

  def items
    [
      Item.new('001', 'Travel Card Holder', 9.25),
      Item.new('002', 'Personalised cufflinks', 45.00),
      Item.new('003', 'Kids T-shirt', 19.95),
    ]
  end
end
